// Test default example :
describe('The SportNow site home page', () => {
    it('successfully loads', () => {
        cy.visit('http://localhost:3000/')
    })
})

describe('Test', () => {
    it('Result /test', () => {
        cy.visit('http://localhost:3000/test')
        cy.get('td').should('have.length', 70)
        cy.get('tr').should('have.length', 11)
        cy.get(':nth-child(10) > :nth-child(1)').contains("Stade du Noir et Or");
        cy.get('thead > tr > :nth-child(1)').contains("Name");
    })
})

/* Only on local */
/*
describe('Form', () => {
    it('Test address', () => {
        cy.visit('http://localhost:3000/')
        cy.get('#search').clear().type('Calais')
        cy.get('form > .btn').click()
        cy.url().should('eq',"http://localhost:3000/resultat?search=Calais&coordinates=&sport=-1&rayon=1")
    })
    it('Test slider', () => {
        cy.visit('http://localhost:3000/')
        cy.get('input[type=range]').invoke('val', 99).trigger('change')
        cy.get('#rayon-label').contains("99")
        cy.get('input[type=range]').invoke('val').should('eq', '99')
    })
})
*/