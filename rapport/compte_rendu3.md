# Compte rendu du Vendredi 08/01/2021

## Matin

### Daily Meeting

Chacun a travaillé de son côté sur le projet, le déploiement sur Heroku est fait, le formulaire a été retravaillé : il est désormais possible de rechercher avec une adresse (Ville Code-Postal par exemple). Il faut que l'on règle quelques problèmes (pipeline qui n'est pas passée à cause des tests Cypress, problème d'affichage des résultats) avant de sortir une release.

### Sprint Planning

Nom | Prénom | Tâche à réaliser
--- | --- | ---
**Benrabh** | Farouk | Redirection du tableau de résultats sur une autre page
**Abdellah** | Sami | Amélioration et correction du formulaire
**Decocq** | Benoit | Générer et déployer documentation technique
**Deswarte** | Quentin | Tests Cypress

### Sprint Review

A la fin de ce sprint, nous avons réussi à rediriger les résultats sur une autre page, il faudra embellir le tableau durant le dernier sprint. La documentation technique est déployée sur Gitlab, on peut y accéder depuis le lien dans la partie "Documentation" du README.
Finalement nous avons pas eu le temps de faire les test Cypress, il fallait en priorité faire la redirection et le tableau, donc Quentin a aidé Farouk sur cette tâche. Enfin nous avons eu des problèmes pour fusionner le code produit par chaque membre, il faut que l'on règle ce point dans le sprint de cet après-midi.

## Après-midi

### Sprint Planning

Pour le dernier sprint, nous voulons avoir une version fonctionnelle et propre, c'est-à-dire que nous devons embellir à la fois le code et le site.
Il faut aussi que l'on règle les problèmes de fusion entre les différentes branches.
Une fois les points ci-dessus réalisés, nous pourrons sortir une release.

### Sprint Review

Les objectifs ont été atteints, le code et le site ont été rendus propres. Avec le peu de temps qu'il nous restait nous n'avons pas pu commencer à implémenter de nouvelles fonctionnalités.

### Retrospective

Bien | Mauvais
--- | ---
Projet fonctionnel | Problèmes d'organisation pour la gestion des branches (fusion des branches)
Grosse productivité dans la journée | 