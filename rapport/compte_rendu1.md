# Compte rendu du Mercredi 06/01/2021

## Matin

### Daily Meeting

Première réunion du projet, on parle des sujets, et avons choisis le sujet numéro 1, qui consiste à développer un nouveaux service pour permettre à des clients de rechercher des lieux permettant la pratique de sport.

## Après-midi

Tout d'abord, nous avons dû réaliser un Sprint Planning, qui va servir à déterminer les tâches que les membres de l'équipe vont faire durant le prochain sprint.

### Sprint planning

Nom | Prénom | Tâche à réaliser
--- | --- | ---
**Benrabh** | Farouk | Réalisation d’une page web pour pouvoir récupérer les données de l'utilisateur
**Abdellah** | Sami | Réalisation d’une page web pour pouvoir récupérer les données de l'utilisateur
**Decocq** | Benoit | Préparation de l'environnement de travail et déploiement Gitlab CI
**Deswarte** | Quentin | Initialisation sur les API

### Sprint Review

Les tâches à réaliser sont toutes terminées, nous sommes dans les temps sur ce que nous avions prévu avec le planning.

### Retrospective

Bien | Mauvais
--- | ---
Formulaire créé | Beaucoup de temps perdu sur l'appel API
Architecture Node.js fonctionnelle | Idem pour le déploiement sur Git 
Tests unitaires et Cypress initialisés |
Déploiement Gitlab CI |