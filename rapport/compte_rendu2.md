# Compte rendu du Jeudi 07/01/2021

## Matin

### Sprint Planning

Nom | Prénom | Tâche à réaliser
--- | --- | ---
**Benrabh** | Farouk | Localisation, travail sur le formulaire, affichage des résultats
**Abdellah** | Sami | Localisation, travail sur le formulaire, affichage des résultats
**Decocq** | Benoit | Récupérer les données de l'API
**Deswarte** | Quentin | Récupérer les données de l'API

### Sprint Review

La localisation est faite, nous avons les résultats de l'API, le formulaire n'est pas totalement fonctionnel, il faut qu'on réussisse à avoir une version fonctionnelle avant la fin du prochain sprint.
L'issue "Initialisation API" est fermée car la fonctionnalité a été implémentée, testée et documentée.

## Après-midi

### Sprint Planning

Travail en binôme sur l'ensemble de l'après-midi (Benrabh-Abdellah / Deswarte-Decocq).

Nom | Prénom | Tâche à réaliser
--- | --- | ---
**Benrabh** | Farouk | Finir le formulaire, rechercher comment utiliser l'API Google pour récupérer la latitude et longitude en entrant le nom d'une ville et son pays
**Abdellah** | Sami | Finir le formulaire, rechercher comment utiliser l'API Google pour récupérer la latitude et longitude en entrant le nom d'une ville et son pays
**Decocq** | Benoit | Finir le tableaux de la page de résultat et fermer l'issue, puis chercher comment déployer sur Herokuapp
**Deswarte** | Quentin | Finir le tableaux de la page de résultat et fermer l'issue, puis chercher comment déployer sur Herokuapp

### Sprint Review

Nous avons réalisé les tâches prévues et fermé les issues correspondantes après les avoir implémentées testées et documentées.

### Retrospective

Bien | Mauvais
--- | ---
Formulaire mis à jour | Plus de communication
Tableaux avec toutes les valeurs récupérées | Plus d'entraide
