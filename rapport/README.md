# Liste des comptes rendus

Ce document recense tous les comptes rendus effectués durant le projet (sprint planning, daily meeting, sprint review, sprint retrospective). Ils sont séparés en fonction du jour auquel ils ont été réalisés.

* [Compte rendu du Mercredi 06/01/2021](./compte_rendu1.md)
* [Compte rendu du Jeudi 07/01/2021](./compte_rendu2.md) 
* [Compte rendu du Vendredi 08/01/2021](./compte_rendu3.md)