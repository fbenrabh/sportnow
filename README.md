# SportNow

L'équipe **SportNow** est composée des membres suivants :
* Quentin Deswarte
* Farouk Benrabh
* Sami Abdellah
* Benoit Decocq

## But du projet

Déployer un service permettant d'obtenir les lieux de sport en fonction de la position du rayon et du sport.

### Fonctionnalités

 - Formulaire d'accueil permettant de renseigner une ville ou la localisation de l'utilisateur, de définir un rayon de recherche et du type de sport recherché.

 - Affichage des résultats sous forme de tableau comprenant les informations suivantes:

    * Le nom du lieu
    * La distance en km qui sépare le lieu et la localisation entrée par l'utilisateur
    * L'addresse du lieu
    * Le numéro de téléphone du responsable
    * Son nom et prenom

## Comptes rendus des réunions

Les comptes rendus des réunion sont disponibles [ici](./rapport/README.md).

## Definition Of Done

Une tâche est validée lorsqu'elle est :
* Implémentée
* Documentée
* Testée

## Comment lancer le projet

### Utiliser en ligne

Le projet **SportNow** est déployé en ligne sur Herokuapp. Il y a deux versions : une version pour la branche *dev* qui permet de tester que nos modifications fonctionnent bien en ligne, et une version pour la branche *master* qui sert à mettre en production.
* [Lien de la version de développement](https://m1-sportnow-staging.herokuapp.com/)
* [Lien de la version de production](https://m1-sportnow.herokuapp.com/)

### Cloner le projet

```
git clone https://gitlab.com/DecocqB/sportnow.git
cd sportnow
```

### Lancer les pages principales

```
npm install
npm run devstart
```

Go to `http://localhost:3000/`

### Lancer les tests unitaires

```
cd test
npm install
npm run test

```

### Lancer les tests cypress

Dans le répertoire `./sportnow` on fait : `npm install`, puis dans 2 terminaux différents :
* Dans le premier : `npm start`
* Dans le deuxième : `./node_modules/.bin/cypress run`

## Analyse SonarCloud

L'analyse du code par SonarCloud est disponible [ici](https://sonarcloud.io/dashboard?id=DecocqB_sportnow).

## Documentation

La documentation technique est disponible [ici](https://decocqb.gitlab.io/sportnow).