
/**
 * global variable to store (x,y) origin
 */
var COORDINATES = {lati:'',longi:''};
	
/**
 * Order query to Decathlon API by giving coordinates, sport (optional) and radius
 * @param {String} origin Coordinates : longitude,latitude
 * @param {*} sport Sport ID (example : 175 is the sport id for ice hockey)
 * @param {*} radius km around my latitude and longtitude
 */
function requestWithParameter(origin,sport,radius){ 
    var request = new XMLHttpRequest();
    var sport_param = '';
    if(sport != '-1')  sport_param = '&sports='+sport;
    var url = "https://sportplaces.api.decathlon.com/api/v1/places?origin="+origin+"&radius="+radius+sport_param;
    request.open('GET', url);
    request.responseType = 'json';
    request.send();

    request.onreadystatechange =  function() {
        if (this.readyState == XMLHttpRequest.DONE && this.status == 200) {
            processData(this.response.data.features);
        }
    };
}

/**
 * Process query results and show them in the page
 * @param {*} data Query result
 */
function processData(data){
    var table = document.createElement('table');
    table.setAttribute("class", "table table-dark");
    var thead = document.createElement('thead');
    var tr = document.createElement('tr');
    headName(tr, "Name");
    headName(tr, "Proximity");
    headName(tr, "Address");
    headName(tr, "City");
    headName(tr, "Phone");
    headName(tr, "First Name");
    headName(tr, "Last Name");
    thead.appendChild(tr);
    var tbody = document.createElement('tbody');
    for(var i = 0; i < data.length; i++){
        var dataProperties = data[i].properties;
        var tr = document.createElement("tr");
        
        createCell(tr, dataProperties.name);
        createCell(tr, dataProperties.proximity);
        createCell(tr, dataProperties.address_components.address);
        createCell(tr, dataProperties.address_components.city);
        createCell(tr, dataProperties.contact_details.phone);
        createCell(tr, dataProperties.user.first_name);
        createCell(tr, dataProperties.user.last_name);
        if(dataProperties.name!=='Under review, proposed: -' && dataProperties.name!=='-')
        tbody.appendChild(tr);
        console.log(dataProperties.name);
    }
    
    table.append(thead, tbody);
    document.getElementById('table').innerHTML = "";
    document.getElementById('table').appendChild(table);

    /**
     * Create cell in tbody with the label wanted
     * @param {*} tr Table row
     * @param {*} label Text given by the API for a field
     */
    function createCell(tr, label) {
        var td = document.createElement("td");
        if (label == null) label = "Unspecified";
        var tdText = document.createTextNode(label);
        td.appendChild(tdText);
        tr.appendChild(td);
    }

    /**
     * Create header cells with label 
     * @param {*} tr Header row
     * @param {*} label Header name
     */
    function headName(tr, label) {
        var th = document.createElement('th');
        var thText = document.createTextNode(label);
        th.appendChild(thText);
        tr.appendChild(th);
    }
}

/**
 * Order query to Decathlon API in order to get the sports list
 */
function requestSports(){ 

    var request = new XMLHttpRequest();
    request.open('GET','https://sports.api.decathlon.com/sports?parents_only=true');
    request.responseType = 'json';
    request.send();


    request.onreadystatechange = function() {
        if (this.readyState == XMLHttpRequest.DONE && this.status == 200) {
            processDataSports(this.response.data);
            sports = this.response.data;
        }
    };
}

/**
 * Show the sports list in "select" form 
 * @param {*} data Query data containing the sports list 
 */
function processDataSports(data){
    var option = '<select  id="selected" class="form-control" name="sport" >';
    option += '<option value="-1"></option>';
                                     
    for(var i = 0; i < data.length; i++){
        option += '<option value="'+data[i].id+'" >'+data[i].attributes.name+'</option>';
        
    }
    option += ' </select>';
document.getElementById('sports-id').innerHTML = option;
};

/**
 * Get the parameters given by the form
 */
function recupParametre(){
    var tabParametres=new Array();
    //var tabParametres={};
    var parametres = location.search.substring(1).split("&");
    console.log(parametres);
    for(var i = 0; i < parametres.length; i++){
        var temp = parametres[i].split("=");
        //tabParametres.push(temp[1]);
        tabParametres[temp[0]] = temp[1];
    }
    return tabParametres;
}

/**
 * Update the radius label to match the type "range" field
 */
function updateRayonValue(){
	var r = document.getElementById("rayon").value;
	document.getElementById("rayon-label").innerHTML='Radius: '+r;
}

/**
 * Get the coordinates (longitude, latitude) from an adress given by the user
 * @param {String} adresse Adress the user asks (example : Calais 62100)
 * @param {*} r Radius
 * @param {*} sport Sport ID (optional)
 */
function requestGetGeoLocalFromAdresse(adresse,r,sport){ 
    var request = new XMLHttpRequest();
    request.open('GET', 'https://api-adresse.data.gouv.fr/search/?q='+adresse);
    request.responseType = 'json';
    request.send();

    request.onreadystatechange =  function() {
        if (this.readyState == XMLHttpRequest.DONE && this.status == 200) {
            var c = processDataGeoLocal(this.response.features);
            COORDINATES.longi = c.longi;
            COORDINATES.lati = c.lati;
            
            var origin = COORDINATES.longi+','+COORDINATES.lati;
            console.log(origin);
            requestWithParameter(origin,sport,r);    
        }
    };
}

/**
 * Get coordinates
 * @param {*} data Query response data
 */
function processDataGeoLocal(data){
                       
     var x =data[0].geometry.coordinates[0];
     var y =data[0].geometry.coordinates[1];
     var coor = {lati:y,longi:x};
     return coor;
};

/**
 * Get the sport places
 */
function getPlaces(){

var params = recupParametre();
console.log(params);
var search = params['search'];
var origin = params['coordinates'];
var sport = params['sport'];
var  r = params['rayon'];

if (typeof origin !== 'undefined' && origin !=='') {
     requestWithParameter(origin,sport,r);
}else{
	requestGetGeoLocalFromAdresse(search,r,sport);
}
 


	
}

/**
 * Get coordinates of the adress given
 * @param {*} search Adress
 */
function getOrigin(search){
	requestGetGeoLocalFromAdresse(search);
}
